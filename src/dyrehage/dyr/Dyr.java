package dyrehage.dyr;

import dyrehage.rom.Rom;
import java.io.Serializable;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public abstract class Dyr implements DyrInterface, Serializable{

    private String navn = null;
    private String art = null;
    private String leveomr = null;
    private String laereProg = "Det er enda ikke opprettet et læreprogram for denne dyrearten.";
    private Rom rom = null;

    /**
     * Get the value of rom
     *
     * @return the value of rom
     */
    @Override
    public Rom getPlassering() {
        return rom;
    }

    /**
     * Set the value of rom
     *
     * @param rom new value of rom
     */
    @Override
    public void setPlassering(Rom rom) {
        this.rom = rom;
    }


    /**
     * Get the value of læreprogramm
     *
     * @return the value of læreprog
     */
    @Override
    public String getLaereProg() {
        return laereProg;
    }

    /**
     * Set the value of læreprogramm
     *
     * @param laereProg new value of laereProg
     */
    @Override
    public void setLaereProg(String laereProg) {
        this.laereProg = laereProg;
    }


    /**
     * Get the value of leveområde
     *
     * @return the value of leveomr
     */
    @Override
    public String getLeveomr() {
        return leveomr;
    }

    /**
     * Set the value of leveområde
     *
     * @param leveomr new value of leveomr
     */
    @Override
    public void setLeveomr(String leveomr) {
        this.leveomr = leveomr;
    }


    /**
     * Get the value of art
     *
     * @return the value of art
     */
    @Override
    public String getArt() {
        return art;
    }

    /**
     * Set the value of art
     *
     * @param art new value of art
     */
    @Override
    public void setArt(String art) {
        this.art = art;
    }


    /**
     * Get the value of navn
     *
     * @return the value of navn
     */
    @Override
    public String getNavn() {
        return navn;
    }

    /**
     * Set the value of navn
     *
     * @param navn new value of navn
     */
    @Override
    public void setNavn(String navn) {
        this.navn = navn;
    }

    
}
