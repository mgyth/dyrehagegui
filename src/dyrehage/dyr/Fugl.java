/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage.dyr;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class Fugl extends Dyr{

    private int minEgg = 0;
    private int maxEgg = 0;

    /**
     * Get the value of maxEgg
     *
     * @return the value of maxEgg
     */
    public int getMaxEgg() {
        return maxEgg;
    }

    /**
     * Set the value of maxEgg, maxEgg må være større eller lik minEgg
     *
     * @param maxEgg new value of maxEgg
     */
    public void setMaxEgg(int maxEgg) {
        if(maxEgg < minEgg){
            this.maxEgg = this.minEgg;
        } else {
            this.maxEgg = maxEgg;
        }
    }


    /**
     * Get the value of minEgg
     *
     * @return the value of minEgg
     */
    public int getMinEgg() {
        return minEgg;
    }

    /**
     * Set the value of minEgg, minEgg må være 0 eller større, og mindre enn maxEgg
     *
     * @param minEgg new value of minEgg
     */
    public void setMinEgg(int minEgg) {
        if(minEgg < 0 && minEgg > this.maxEgg){
            this.minEgg = 0;
        } else {
            this.minEgg = minEgg;
        }
        
    }
    
    @Override
    public String toString(){
        return "Fugl: " + this.getNavn() + " - " + this.getArt();
    }

}
