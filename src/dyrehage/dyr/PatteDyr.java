/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage.dyr;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class PatteDyr extends Dyr{

    private boolean farlig = false;

    /**
     * Get the value of farlig
     *
     * @return the value of farlig
     */
    public boolean isFarlig() {
        return farlig;
    }

    /**
     * Set the value of farlig
     *
     * @param farlig new value of farlig
     */
    public void setFarlig(boolean farlig) {
        this.farlig = farlig;
    }
    
    @Override
    public String toString(){
        String erFarlig = this.isFarlig() ? "ja" : "nei";
        return "Pattedyr: "  + this.getNavn() + " - " + this.getArt();
    }

}
