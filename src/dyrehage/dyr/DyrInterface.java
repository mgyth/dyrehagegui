package dyrehage.dyr;

import dyrehage.rom.Rom;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public interface DyrInterface {
    /**
     * Sett navnet på dyret
     * @param navn
     */
    public void setNavn(String navn);
    /**
     * Hent navnet på dyret
     * @return navnet dyret skal ha
     */
    public String getNavn();
    
    /**
     * 
     * @param art
     */
    public void setArt(String art);
    /**
     * 
     * @return
     */
    public String getArt();
    
    /**
     * 
     * @param leveomr
     */
    public void setLeveomr(String leveomr);
    /**
     * 
     * @return
     */
    public String getLeveomr();
    
    /**
     * 
     * @param laereProgr
     */
    public void setLaereProg(String laereProgr);
    /**
     * 
     * @return
     */
    public String getLaereProg();
    
    /**
     * 
     * @param rom
     */
    public void setPlassering(Rom rom);
    /**
     * 
     * @return
     */
    public Rom getPlassering();
}
