package dyrehage;

import dyrehage.dyr.Dyr;

import dyrehage.rom.Rom;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class Dyrehagen implements Serializable {

    private  ArrayList<Rom> rom;
    private  ArrayList<Dyr> dyr;
    public  ArrayList<Dyr> getDyrListe() {
        return dyr;
    }

    public ArrayList<Rom> getRomListe() {
        return rom;
    }
    

    public Dyrehagen() {
        rom = new ArrayList();
        dyr = new ArrayList();
    }
}
