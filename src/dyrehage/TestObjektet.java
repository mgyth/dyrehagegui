/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage;

import dyrehage.dyr.Dyr;
import dyrehage.dyr.PatteDyr;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
class TestObjektet implements Serializable{
    private static ArrayList<Dyr> dyr = new ArrayList();
    private PatteDyr nyttDyr = null;

    public static ArrayList<Dyr> getDyr() {
        return dyr;
    }

    public TestObjektet() {
            
        nyttDyr = new PatteDyr();
        nyttDyr.setNavn("ole");
        nyttDyr.setArt("Ape");
        nyttDyr.setLeveomr("Jungelen");
        nyttDyr.setFarlig(true);
        
        dyr.add(nyttDyr);
    }

    
}
