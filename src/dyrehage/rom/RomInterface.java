package dyrehage.rom;

import dyrehage.dyr.Dyr;
import java.util.ArrayList;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public interface RomInterface {

    
    public enum Type {

        PATTEDYR,
        REPTIL,
        FUGL
    };
    
    public enum Avd {

        NORD,
        SØR,
        ØST,
        VEST
    };
    
    /**
     * 
     * @param navn
     */
    public void setNavn(String navn);
    /**
     * 
     * @return
     */
    public String getNavn();
    
    /**
     * 
     * @param type
     */
    public void setType(RomInterface.Type type);
    /**
     * 
     * @return
     */
    public RomInterface.Type getType();
    
    /**
     * 
     * @param avd
     */
    public void setAvd(RomInterface.Avd avd);
    /**
     * 
     * @return
     */
    public RomInterface.Avd getAvd();
    
    /**
     * 
     * @param farlig
     */
    public void setFarlig(boolean farlig);
    /**
     * 
     * @return
     */
    public boolean hasFarlig();
    
    /**
     * 
     * @param dyr
     */
    public void addDyr(Dyr dyr);
    /**
     * 
     * @param dyr
     */
    public void removeDyr(Dyr dyr);
    /**
     * 
     * @return
     */
    public ArrayList getDyr();
    
    /**
     * 
     * @return
     */
    public boolean isEmpty();
}
