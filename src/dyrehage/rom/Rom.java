package dyrehage.rom;

import dyrehage.dyr.Dyr;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class Rom implements RomInterface, Serializable{
    private boolean farlig = false;
    private int antall = 0;
    private Type type = null;
    private Avd avd = null;
    private ArrayList dyr = new ArrayList();
    private String navn = null;

    @Override
    public boolean hasFarlig() {
        return farlig ? true : false;
    }
    
    @Override
    public void setFarlig(boolean farlig) {
        this.farlig = farlig;
    }

    @Override
    public boolean isEmpty() {
        return antall > 0 ? false : true;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public Type getType() {
        return this.type;
    }

    @Override
    public void setAvd(Avd avd) {
        this.avd = avd;
    }

    @Override
    public Avd getAvd() {
        return this.avd;
    }

    @Override
    public void addDyr(Dyr dyr) {
        this.dyr.add(dyr);
        this.antall++;
    }

    @Override
    public void removeDyr(Dyr dyr) {
        this.dyr.remove(dyr);
        this.antall--;
    }

    @Override
    public ArrayList getDyr() {
        return this.dyr;
    }

    @Override
    public void setNavn(String navn) {
        this.navn = navn;
    }

    @Override
    public String getNavn() {
        return this.navn;
    }
    
    @Override
    public String toString(){
        String tomt = isEmpty() ? "Ja" : "Nei";
        return navn + " - " + avd;
    }
}
