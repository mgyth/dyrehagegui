/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage;

import dyrehage.meny.GUI;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class Dyrehage {

    private static Dyrehagen dh = new Dyrehagen();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Prøv å les objektet fra fil.
        dh = lesFraFil();

        // hvis objektet er null så opprett ett nytt et
        if (dh == null) {
            dh = new Dyrehagen();
        }

        new GUI(dh).setVisible(true);
    }

    /**
     * Metode for å lese objektet fra fil
     *
     * @return Dyrehagen objektet som er lagret til fil
     */
    public static Dyrehagen lesFraFil() {
        Object objektet = null;
        FileInputStream innStrøm = null;
        ObjectInputStream inn = null;
        //File file = new File("dyrehagen.ser");

        try {

            try {
                innStrøm = new FileInputStream("Dyrehagen.ser");
            } catch (FileNotFoundException ex) {
                //Logger.getLogger(Dyrehagen.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Fant ikke filen.");
            }

            if (innStrøm != null) {
                inn = new ObjectInputStream(innStrøm);
            }

            try {
                if (inn != null) {
                    objektet = inn.readObject();
                    inn.close();
                }

            } catch (ClassNotFoundException ex) {
                //Logger.getLogger(Dyrehagen.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            //Logger.getLogger(Dyrehagen.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Fikk ikke kontakt med filen...");
        }
        return (Dyrehagen) objektet;
    }

    /**
     * Metode for å lagre objekter til fil
     */
    public static void lagreTilFil() {
        try {
            FileOutputStream utStrm = null;
            ObjectOutputStream ut;
            String navn = "Dyrehagen.ser";
            try {
                File filen = new File(navn);
                utStrm = new FileOutputStream(filen);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Dyrehagen.class.getName()).log(Level.SEVERE, null, ex);
            }
            ut = new ObjectOutputStream(utStrm);
            ut.writeObject(dh); // lagre dh objektet opprettet i
            ut.close();
        } catch (IOException ex) {
            Logger.getLogger(Dyrehagen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
