/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage.meny;

import dyrehage.Dyrehagen;
import dyrehage.dyr.Dyr;
import dyrehage.rom.Rom;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
class MainP extends JPanel {

    private JPanel nPanel = new JPanel();
    private JPanel sPanel = new JPanel();
    private JPanel ePanel = new JPanel();
    private JPanel wPanel = new JPanel();
    private JPanel cPanel = new JPanel();
    private JLabel navnLabel = new JLabel("Velg et dyr eller en art for å se info.");
    private JComboBox navnCombo = new JComboBox();
    private JTextArea infoText = new JTextArea("");
    private ArrayList dyr;
    private ArrayList rom;
    private ArrayList nListe = new ArrayList();
    private ArrayList sListe = new ArrayList();
    private ArrayList wListe = new ArrayList();
    private ArrayList eListe = new ArrayList();

    public MainP(Dyrehagen dh) {
        dyr = dh.getDyrListe();
        rom = dh.getRomListe();
        setLayout(new BorderLayout());

        for (Iterator it = rom.iterator(); it.hasNext();) {
            Rom next = (Rom) it.next();
            if (next.getAvd().name().equals("NORD")) {
                nListe.add(next);
            } else if (next.getAvd().name().equals("SØR")) {
                sListe.add(next);
            } else if (next.getAvd().name().equals("VEST")) {
                wListe.add(next);
            } else if (next.getAvd().name().equals("ØST")) {
                eListe.add(next);
            }
        }

        nPanel.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        sPanel.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        ePanel.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        wPanel.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        //-------------------- NORTH panel --------------------//
        nPanel.setPreferredSize(new Dimension(400, 100));
        for (Iterator nit = nListe.iterator(); nit.hasNext();) {
            Rom nRom = (Rom) nit.next();
            JTextPane romP = new JTextPane();
            romP.setBackground(wPanel.getBackground());
            romP.setBorder(BorderFactory.createTitledBorder(null, nRom.getNavn(), TitledBorder.CENTER, TitledBorder.TOP));
            romP.setEditable(false);
            ArrayList nDyrListe = nRom.getDyr();
            String dyrene = "";
            if (nDyrListe.isEmpty()) {
                dyrene = nRom.getNavn() + " er tomt.";
                romP.setText(dyrene);
            } else {
                for (Iterator nDyrIt = nDyrListe.iterator(); nDyrIt.hasNext();) {
                    Dyr nDyr = (Dyr) nDyrIt.next();
                    dyrene += nDyr.toString() + "\n";
                }
                romP.setText(dyrene);
            }
            nPanel.add(romP);
        }
        add(nPanel, BorderLayout.NORTH);

        //-------------------- SOUTH panel --------------------//
        sPanel.setPreferredSize(new Dimension(400, 100));
        for (Iterator nit = sListe.iterator(); nit.hasNext();) {
            Rom sRom = (Rom) nit.next();
            JTextPane romP = new JTextPane();
            romP.setBackground(wPanel.getBackground());
            romP.setBorder(BorderFactory.createTitledBorder(null, sRom.getNavn(), TitledBorder.CENTER, TitledBorder.TOP));
            romP.setEditable(false);
            ArrayList sDyrListe = sRom.getDyr();
            String dyrene = "";
            if (sDyrListe.isEmpty()) {
                dyrene = sRom.getNavn() + " er tomt.";
                romP.setText(dyrene);
            } else {
                for (Iterator sDyrIt = sDyrListe.iterator(); sDyrIt.hasNext();) {
                    Dyr sDyr = (Dyr) sDyrIt.next();
                    dyrene += sDyr.toString() + "\n";
                }
                romP.setText(dyrene);
            }
            sPanel.add(romP);
        }
        add(sPanel, BorderLayout.SOUTH);

        //-------------------- EAST panel --------------------//
        ePanel.setPreferredSize(new Dimension(150, 100));
        for (Iterator nit = eListe.iterator(); nit.hasNext();) {
            Rom eRom = (Rom) nit.next();
            JTextPane romP = new JTextPane();
            romP.setBackground(wPanel.getBackground());
            romP.setBorder(BorderFactory.createTitledBorder(null, eRom.getNavn(), TitledBorder.CENTER, TitledBorder.TOP));
            romP.setEditable(false);
            ArrayList eDyrListe = eRom.getDyr();
            String dyrene = "";
            if (eDyrListe.isEmpty()) {
                dyrene = eRom.getNavn() + " er tomt.";
                romP.setText(dyrene);
            } else {
                for (Iterator eDyrIt = eDyrListe.iterator(); eDyrIt.hasNext();) {
                    Dyr eDyr = (Dyr) eDyrIt.next();
                    dyrene += eDyr.toString() + "\n";
                }
                romP.setText(dyrene);
            }
            ePanel.add(romP);
        }
        add(ePanel, BorderLayout.EAST);

        //-------------------- WEST panel --------------------//
        wPanel.setPreferredSize(new Dimension(150, 100));
        for (Iterator nit = wListe.iterator(); nit.hasNext();) {
            Rom wRom = (Rom) nit.next();
            JTextPane romP = new JTextPane();
            romP.setBackground(wPanel.getBackground());
            romP.setBorder(BorderFactory.createTitledBorder(null, wRom.getNavn(), TitledBorder.CENTER, TitledBorder.TOP));
            romP.setEditable(false);
            ArrayList wDyrListe = wRom.getDyr();
            String dyrene = "";
            if (wDyrListe.isEmpty()) {
                dyrene = wRom.getNavn() + " er tomt.";
                romP.setText(dyrene);
            } else {
                for (Iterator wDyrIt = wDyrListe.iterator(); wDyrIt.hasNext();) {
                    Dyr wDyr = (Dyr) wDyrIt.next();
                    dyrene += wDyr.toString() + "\n";
                }
                romP.setText(dyrene);
            }
            wPanel.add(romP);
        }
        add(wPanel, BorderLayout.WEST);

        //-------------------- CENTER panel --------------------//
        for (Iterator navnIt = dyr.iterator(); navnIt.hasNext();) {
            Dyr next = (Dyr) navnIt.next();
            navnCombo.addItem(next);
        }
        ActionListener actionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Dyr dyr = (Dyr) navnCombo.getSelectedItem();
                infoText.setLineWrap(true);
                infoText.setWrapStyleWord(true);
                infoText.setText(dyr.getNavn() + " er en " + dyr.getArt() + ".\n\n"+ dyr.getNavn() + "s naturlige leveområde er " + dyr.getLeveomr() + ".\n\nLæreprogram:\n" + dyr.getLaereProg());
            }
        };
        navnCombo.addActionListener(actionListener);

        cPanel.add(navnLabel);

        cPanel.add(navnCombo);

        JScrollPane infoScroll = new JScrollPane(infoText);
        infoScroll.setPreferredSize(new Dimension(260, 180));
        cPanel.add(infoScroll);

        add(cPanel, BorderLayout.CENTER);
    }
}
