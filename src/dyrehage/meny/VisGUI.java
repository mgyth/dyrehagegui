/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage.meny;

import dyrehage.Dyrehage;
import dyrehage.dyr.Dyr;
import dyrehage.dyr.Fugl;
import dyrehage.dyr.KrypDyr;
import dyrehage.dyr.PatteDyr;
import dyrehage.rom.Rom;
import dyrehage.rom.RomInterface.Avd;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.PatternSyntaxException;
import javax.swing.*;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class VisGUI extends JPanel {

    private JTabbedPane tabPanel = new JTabbedPane();
    private JPanel visTab;
    private JPanel endreTab;
    private Object objektet;
    private String visNavn = "";
    private String endreNavn = "";
    private ArrayList romListe;
    private ArrayList dyrListe;
    private GUI gui;
    private final String _REGXSTRING = "(?i)[\\wæøå(),.\\-\\s]+";

    public VisGUI(Object objektet, ArrayList rom, ArrayList dyr, GUI gui) {
        this.objektet = objektet;
        this.gui = gui;
        romListe = rom;
        dyrListe = dyr;

        endreTab = new JPanel();
        if (objektet instanceof PatteDyr) {
            PatteDyr pd = (PatteDyr) objektet;
            visTab = new VisDyrPanel(pd);
            endreTab = new EndreDyrPanel(pd, gui);
            visNavn = "Se på " + pd.getNavn();
            endreNavn = "Endre " + pd.getNavn();

        } else if (objektet instanceof KrypDyr) {
            KrypDyr kd = (KrypDyr) objektet;
            visTab = new VisDyrPanel(kd);
            endreTab = new EndreDyrPanel(kd, gui);
            visNavn = "Se på " + kd.getNavn();
            endreNavn = "Endre " + kd.getNavn();

        } else if (objektet instanceof Fugl) {
            Fugl f = (Fugl) objektet;
            visTab = new VisFugl(f);
            endreTab = new EndreFugl(f, gui);
            visNavn = "Se på " + f.getNavn();
            endreNavn = "Endre " + f.getNavn();

        } else if (objektet instanceof Rom) {
            Rom r = (Rom) objektet;
            visTab = new VisRom(r);
            endreTab = new EndreRom(r);
            visNavn = "Se på " + r.getNavn();
            endreNavn = "Endre " + r.getNavn();

        }
        visTab.setPreferredSize(new Dimension(578, 406));
        tabPanel.addTab(visNavn, visTab);
        endreTab.setPreferredSize(new Dimension(578, 406));
        tabPanel.addTab(endreNavn, endreTab);
        tabPanel.setVisible(true);
        add(tabPanel);

    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om PatteDyr og KrypDyr objekter
     * @param Dyr en subklasse av Dyr; PatteDyr eller KrypDyr
     */
    class VisDyrPanel extends JPanel {

        private Dyr dyret;
        private JLabel typeLabel = new JLabel("Type:");
        private JTextField typeText = new JTextField();
        private JLabel artLabel = new JLabel("Art:");
        private JTextField artText = new JTextField();
        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel farlLabel = new JLabel("Farlig:");
        private JTextField farlText = new JTextField();
        private JLabel omrLabel = new JLabel("Leveområde:");
        private JTextField omrText = new JTextField();
        private JLabel prgLabel = new JLabel("Læreprogram:");
        private JTextArea prgText = new JTextArea();
        private JLabel plassLabel = new JLabel("Plassering:");
        private JTextField plassText = new JTextField();

        public VisDyrPanel(Dyr dyret) {
            setLayout(new GridLayout(0, 2));
            String type = "Ukjent";
            String farlig = "Ukjent";
            if (dyret instanceof PatteDyr) {
                PatteDyr pd = (PatteDyr) dyret;
                type = "Pattedyr";
                farlig = pd.isFarlig() == true ? "Ja" : "Nei";
            } else if (dyret instanceof KrypDyr) {
                KrypDyr kd = (KrypDyr) dyret;
                type = "Krypdyr";
                farlig = kd.isFarlig() == true ? "Ja" : "Nei";
            }

            add(typeLabel);
            typeText.setText(type);
            typeText.setEditable(false);
            add(typeText);

            add(artLabel);
            artText.setText(dyret.getArt());
            artText.setEditable(false);
            add(artText);

            add(navnLabel);
            navnText.setText(dyret.getNavn());
            navnText.setEditable(false);
            add(navnText);

            add(farlLabel);
            farlText.setText(farlig);
            farlText.setEditable(false);
            add(farlText);

            add(omrLabel);
            omrText.setText(dyret.getLeveomr());
            omrText.setEditable(false);
            add(omrText);

            add(prgLabel);
            prgText.setText(dyret.getLaereProg());
            prgText.setBackground(this.getBackground());
            prgText.setBorder(omrText.getBorder());
            prgText.setLineWrap(true);
            prgText.setWrapStyleWord(true);
            prgText.setEditable(false);
            add(prgText);

            add(plassLabel);
            plassText.setText(dyret.getPlassering().getNavn());
            plassText.setEditable(false);
            add(plassText);
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om PatteDyr og KrypDyr objekter som kan redigeres og lagres til fil.
     * @param Dyr en subklasse av Dyr; PatteDyr eller KrypDyr
     */
    class EndreDyrPanel extends JPanel {

        private Dyr dyret;
        private JLabel artLabel = new JLabel("Art:");
        private JTextField artText = new JTextField();
        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel farlLabel = new JLabel("Farlig:");
        private JLabel omrLabel = new JLabel("Leveområde:");
        private JTextField omrText = new JTextField();
        private JLabel prgLabel = new JLabel("Læreprogram:");
        private JTextArea prgText = new JTextArea();
        private JLabel plassLabel = new JLabel("Plassering:");
        private JComboBox plassBox = new JComboBox();
        private JButton lagre = new JButton("Lagre endringer");
        private JButton slett = new JButton("Slett");
        private JPanel buttCont = new JPanel();
        private JTextArea errorField = new JTextArea("");
        private JScrollPane scrlPane = new JScrollPane();
        private JCheckBox farligBox = new JCheckBox();
        private GUI gui;

        public EndreDyrPanel(final Dyr dyret, final GUI gui) {
            this.gui = gui;
            setLayout(new GridLayout(0, 2));
            this.dyret = dyret;
            String farlig = "Ukjent";
            if (dyret instanceof PatteDyr) {
                PatteDyr pd = (PatteDyr) dyret;
                farligBox.setSelected(pd.isFarlig());
            } else if (dyret instanceof KrypDyr) {
                KrypDyr kd = (KrypDyr) dyret;
                farligBox.setSelected(kd.isFarlig());
            }

            add(artLabel);
            artText.setText(dyret.getArt());
            add(artText);

            add(navnLabel);
            navnText.setText(dyret.getNavn());
            add(navnText);

            add(farlLabel);

            farligBox.setText("Farlig");
            add(farligBox);

            add(omrLabel);
            omrText.setText(dyret.getLeveomr());
            add(omrText);

            add(prgLabel);
            prgText.setLineWrap(true);
            prgText.setWrapStyleWord(true);
            prgText.setBorder(omrText.getBorder());
            prgText.setText(dyret.getLaereProg());
            add(prgText);

            add(plassLabel);
            plassBox.addItem(dyret.getPlassering());
            for (Iterator it = romListe.iterator(); it.hasNext();) {
                plassBox.addItem(it.next());
            }
            add(plassBox);

            scrlPane.setAutoscrolls(true);
            errorField.setEditable(false);
            errorField.setLineWrap(true);
            errorField.setWrapStyleWord(true);
            scrlPane.setViewportView(errorField);
            add(scrlPane);

            lagre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    LagreDyrActionPerformed();
                }
            });
            slett.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    SlettDyrActionPerformed(evt);
                }

                private void SlettDyrActionPerformed(ActionEvent evt) {
                    dyret.getPlassering().removeDyr(dyret);
                    dyrListe.remove(dyret);
                    Dyrehage.lagreTilFil();
                    gui.treRebuilder();
                }
            });
            buttCont.add(lagre);
            buttCont.add(slett);
            add(buttCont);
        }

        /**
         * Metode for å lagre endringer på dyr
         */
        private void LagreDyrActionPerformed() {
            String status = "";
            boolean save = true;

            String art = artText.getText();
            if (!art.equals(dyret.getArt())) {
                try {
                    if (art.matches(_REGXSTRING)) {
                        status += "Art endret til " + art + ".\n";
                        dyret.setArt(art);
                    } else {
                        status += "Art inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    dyret.setArt("Midlertidig generisk artsnavn");
                }
            }

            String navn = navnText.getText();
            if (!navn.equals(dyret.getNavn())) {
                try {
                    if (navn.matches(_REGXSTRING)) {
                        status += "Navn endret til " + navn + ".\n";
                        dyret.setNavn(navn);
                    } else {
                        status += "Navn inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    dyret.setNavn("Midlertidig generisk navn");
                }
            }

            Rom rom = (Rom) plassBox.getSelectedItem();  // hent inn rom verdien i tilfelle den endres før vi skal vurdere om rommet er ferlig eller ikke
            boolean erFarlig = farligBox.isSelected();  // Hent inn farlig status slik at vi kan vurdere om vi kan sette dyret inn i et eventuelt nytt rom

            if (!rom.equals(dyret.getPlassering())) {
                dyret.setPlassering(rom);
                if (rom instanceof Rom) {
                    if (rom.isEmpty() || erFarlig == rom.hasFarlig()) {
                        status += "Plassering satt til " + rom.getNavn() + ".\n";

                        dyret.setPlassering(rom);               // Oppdater dyrets plassering
                    } else {
                        status += "Der er ikke lov å blande farlige dyr med dyr som ikke er farlige.\n";
                        save = false;
                    }
                } else {
                    status += "Ugyldig rom";
                    save = false;
                }
            }

            if (dyret instanceof PatteDyr) {
                PatteDyr pd = (PatteDyr) dyret;
                if (erFarlig != pd.isFarlig()) {                                                                    // Hvis farlig er endret
                    if (erFarlig == pd.getPlassering().hasFarlig() || pd.getPlassering().getDyr().size() == 1) {    // og rommet enter en like farlig eller inneholder bare dette dyret
                        String fare = erFarlig ? "farlig" : "ikke farlig";
                        status += "Dyret er endret til et " + fare + " dyr.\n";
                        pd.getPlassering().setFarlig(erFarlig);                                                     //setter vi statusen på rommet til den samme som dyret (i tilfelle det er bare ett dyr der)
                        pd.setFarlig(erFarlig); // og endrer statusen på dyret
                    } else {
                        if (erFarlig != pd.getPlassering().hasFarlig() && pd.getPlassering().getDyr().size() > 1) { // hvis det er flere enn ett dyr og rommet ikke har samme status tillates ikke endringen
                            status += "Det er ikke lov å blande farlige dyr med dyr som ikke er farlige.\n";
                        } else {
                            status += "Erm, hva??";
                        }
                        save = false;
                    }
                }
            } else if (dyret instanceof KrypDyr) {
                KrypDyr kd = (KrypDyr) dyret;
                if (erFarlig != kd.isFarlig()) {
                    if (erFarlig == kd.getPlassering().hasFarlig() || kd.getPlassering().getDyr().size() == 1) {
                        String fare = erFarlig ? "farlig" : "ikke farlig";
                        status += "Dyret er endret til et " + fare + " dyr.\n";
                        kd.getPlassering().setFarlig(erFarlig);
                        kd.setFarlig(erFarlig);
                    } else {
                        if (erFarlig != kd.getPlassering().hasFarlig() && kd.getPlassering().getDyr().size() > 1) {
                            status += "Det er ikke lov å blande farlige dyr med dyr som ikke er farlige.\n";
                        } else {
                            status += "Erm, hva??";
                        }
                        save = false;
                    }
                }
            }



            String omrd = omrText.getText();
            if (!omrd.equals(dyret.getLeveomr())) {
                try {
                    if (omrd.matches(_REGXSTRING)) {
                        status += "Leveområde satt til " + omrd + ".\n";
                        dyret.setLeveomr(omrd);
                    } else {
                        status += "Område inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    dyret.setNavn("Midlertidig generisk område");
                }
            }

            String program = prgText.getText();
            if (!program.equals(dyret.getLaereProg())) {
                try {
                    if (program.matches(_REGXSTRING) || "".equals(program)) {
                        if ("".equals(program)) {
                            status += "Læreprogram satt til: Det er enda ikke opprettet et læreprogram for denne dyrearten.\n";
                            dyret.setLaereProg("Det er enda ikke opprettet et læreprogram for denne dyrearten.");
                        } else {
                            status += "Læreprogram satt til " + program + ".\n";
                            dyret.setLaereProg(program);
                        }

                    } else {
                        status += "Læringsprogram inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    dyret.setNavn("Det er ikke opprettet noe læringsprogram for dette dyret enda.");
                }
            }


            if (save) {
                status += "Dyrehagen ble oppdater.\n";
                errorField.setText(status);
                dyret.getPlassering().removeDyr(dyret); // Slett dyret fra det gamle rommet
                rom.addDyr(dyret);                      // Legg dyret til det nye rommet.
                rom.setFarlig(erFarlig);                // Sett rommet til farlig/ikke farlig om nødvending
                Dyrehage.lagreTilFil();

                gui.treRebuilder();
            } else {
                status += "Dyrehagen ble ikke oppdater.\n";
                errorField.setText(status);
            }
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om et Fugl objekt
     * @param Fugl
     */
    class VisFugl extends JPanel {

        private JLabel typeLabel = new JLabel("Type:");
        private JTextField typeText = new JTextField();
        private JLabel artLabel = new JLabel("Art:");
        private JTextField artText = new JTextField();
        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel eggLabel = new JLabel("Egg:");
        private JTextField eggText = new JTextField();
        private JLabel omrLabel = new JLabel("Leveområde:");
        private JTextField omrText = new JTextField();
        private JLabel prgLabel = new JLabel("Læreprogram:");
        private JTextArea prgText = new JTextArea();
        private JLabel plassLabel = new JLabel("Plassering:");
        private JTextField plassText = new JTextField();

        public VisFugl(Fugl f) {
            setLayout(new GridLayout(0, 2));
            add(typeLabel);
            typeText.setText("Fugl");
            typeText.setEditable(false);
            add(typeText);

            add(artLabel);
            artText.setText(f.getArt());
            artText.setEditable(false);
            add(artText);

            add(navnLabel);
            navnText.setText(f.getNavn());
            navnText.setEditable(false);
            add(navnText);

            add(eggLabel);
            eggText.setText(f.getMinEgg() + " til " + f.getMaxEgg());
            eggText.setEditable(false);
            add(eggText);

            add(omrLabel);
            omrText.setText(f.getLeveomr());
            omrText.setEditable(false);
            add(omrText);

            add(prgLabel);
            prgText.setLineWrap(true);
            prgText.setWrapStyleWord(true);
            prgText.setText(f.getLaereProg());
            prgText.setBackground(this.getBackground());
            prgText.setBorder(omrText.getBorder());
            prgText.setEditable(false);
            add(prgText);

            add(plassLabel);
            try {
                plassText.setText(f.getPlassering().getNavn());
            } catch (NullPointerException e) {
                plassText.setText("Kunne ikke finne plassering.");
            }
            plassText.setEditable(false);
            add(plassText);
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om et Fugl objekt som kan endres.
     * @param Fugl
     */
    class EndreFugl extends JPanel {

        private JLabel artLabel = new JLabel("Art:");
        private JTextField artText = new JTextField();
        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel eggLabel = new JLabel("Egg:");
        private JLabel omrLabel = new JLabel("Leveområde:");
        private JTextField omrText = new JTextField();
        private JLabel prgLabel = new JLabel("Læreprogram:");
        private JTextArea prgText = new JTextArea();
        private JLabel plassLabel = new JLabel("Plassering:");
        private JComboBox plassBox = new JComboBox();
        private JButton lagre = new JButton("Lagre endringer");
        private JButton slett = new JButton("Slett");
        private JPanel buttCont = new JPanel();
        private JScrollPane scrlPane = new JScrollPane();
        private JTextArea errorField = new JTextArea("");
        private JPanel eggContainer = new JPanel();
        private JLabel minLabel = new JLabel("Min. egg");
        private JSpinner minSpinner = new JSpinner();
        private JLabel maxLabel = new JLabel("Max. egg");
        private JSpinner maxSpinner = new JSpinner();
        private Fugl fugl;

        public EndreFugl(Fugl f, final GUI gui) {
            fugl = f;
            setLayout(new GridLayout(0, 2));

            add(artLabel);
            artText.setText(f.getArt());
            add(artText);

            add(navnLabel);
            navnText.setText(f.getNavn());
            add(navnText);

            add(eggLabel);
            minSpinner.setValue(f.getMinEgg());
            maxSpinner.setValue(f.getMaxEgg());
            eggContainer.add(minLabel);
            eggContainer.add(minSpinner);
            eggContainer.add(maxLabel);
            eggContainer.add(maxSpinner);
            add(eggContainer);

            add(omrLabel);
            omrText.setText(f.getLeveomr());
            add(omrText);

            add(prgLabel);
            prgText.setText(f.getLaereProg());
            prgText.setBorder(omrText.getBorder());
            prgText.setLineWrap(true);
            prgText.setWrapStyleWord(true);
            add(prgText);

            add(plassLabel);
            plassBox.addItem(f.getPlassering());
            for (Iterator it = romListe.iterator(); it.hasNext();) {
                plassBox.addItem(it.next());
            }
            add(plassBox);

            scrlPane.setAutoscrolls(true);
            errorField.setEditable(false);
            errorField.setLineWrap(true);
            errorField.setWrapStyleWord(true);
            scrlPane.setViewportView(errorField);
            add(scrlPane);

            lagre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    LagreFuglActionPerformed(evt);
                }
            });

            slett.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    SlettFuglActionPerformed(evt);
                }

                private void SlettFuglActionPerformed(ActionEvent evt) {
                    fugl.getPlassering().removeDyr(fugl);
                    dyrListe.remove(fugl);
                    Dyrehage.lagreTilFil();
                    gui.treRebuilder();
                }
            });
            buttCont.add(lagre);
            buttCont.add(slett);
            add(buttCont);
        }

        /**
         * Metode for å endre Fugl objektet
         *
         * @param evt
         */
        private void LagreFuglActionPerformed(ActionEvent evt) {
            String status = "";
            boolean save = true;

            String art = artText.getText();
            if (!art.equals(fugl.getArt())) {
                try {
                    if (art.matches(_REGXSTRING)) {
                        status += "Art satt til " + art + ".\n";
                        fugl.setArt(art);
                    } else {
                        status += "Art inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    fugl.setNavn("Midlertidig generisk artsnavn");
                }
            }

            String navn = navnText.getText();
            if (!navn.equals(fugl.getNavn())) {
                try {
                    if (navn.matches(_REGXSTRING)) {
                        status += "Navn satt til " + navn + ".\n";
                        fugl.setNavn(navn);
                    } else {
                        status += "Navn inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    fugl.setNavn("Midlertidig generisk navn");
                }
            }

            int minEgg = (int) minSpinner.getValue();
            int maxEgg = (int) maxSpinner.getValue();

            if (minEgg != fugl.getMinEgg() && minEgg < maxEgg) {
                status += "Minimum egg er endret til " + minEgg + ".\n";
                fugl.setMinEgg(minEgg);
            } else if (minEgg > maxEgg) {
                status += "Kan ikke sette minimum egg høyere enn maksimum egg.";
                save = false;
            }
            if (maxEgg != fugl.getMaxEgg() && maxEgg > minEgg) {
                status += "Maksimum egg endret til " + maxEgg + ".\n";
                fugl.setMaxEgg(maxEgg);
            } else if (maxEgg < minEgg) {
                status += "Kan ikke sette maksimum egg lavere en minimum egg.";
                save = false;
            }

            String omrd = omrText.getText();
            if (!omrd.equals(fugl.getLeveomr())) {
                try {
                    if (omrd.matches(_REGXSTRING)) {
                        status += "Leveområde satt til " + omrd + ".\n";
                        fugl.setLeveomr(omrd);
                    } else {
                        status += "Område inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    fugl.setNavn("Midlertidig generisk område");
                }
            }

            String program = prgText.getText();
            if (!program.equals(fugl.getLaereProg())) {
                try {
                    if (program.matches(_REGXSTRING) || "".equals(program)) {
                        if ("".equals(program)) {
                            status += "Læreprogram satt til: Det er enda ikke opprettet et læreprogram for denne dyrearten.\n";
                            fugl.setLaereProg("Det er enda ikke opprettet et læreprogram for denne dyrearten.");
                        } else {
                            status += "Læreprogram satt til " + program + ".\n";
                            fugl.setLaereProg(program);
                        }
                    } else {
                        status += "Læringsprogram inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    fugl.setNavn("Det er ikke opprettet noe læringsprogram for dette dyret enda.");
                }
            }

            Rom rom = (Rom) plassBox.getSelectedItem();
            if (!rom.equals(fugl.getPlassering())) {
                if (rom instanceof Rom) {
                    status += "Plassering satt til " + rom.getNavn() + ".\n";

                    fugl.setPlassering(rom);              // Oppdater dyrets plassering
                } else {
                    status += "Ugyldig rom";
                    save = false;
                }
            }

            if (save) {
                status += "Dyrehagen ble oppdater.\n";
                errorField.setText(status);
                fugl.getPlassering().removeDyr(fugl); // slett dyret fra det gamle rommet
                rom.addDyr(fugl);                     //legg dyret til det nye rommet.
                Dyrehage.lagreTilFil();

                gui.treRebuilder();
            } else {
                status += "Dyrehagen ble ikke oppdater.\n";
                errorField.setText(status);
            }
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om et Rom objekt
     * @param Rom
     */
    class VisRom extends JPanel {

        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel avdLabel = new JLabel("Avdeling:");
        private JTextField avdText = new JTextField();
        private JLabel fareLabel = new JLabel("Inneholder farlige dyr:");
        private JTextField fareText = new JTextField();
        private JLabel dyrLabel = new JLabel("Dyrene i dette rommet:");
        private JTextArea dyrText = new JTextArea();

        public VisRom(Rom r) {
            setLayout(new GridLayout(0, 2));

            add(navnLabel);
            navnText.setText(r.getNavn());
            navnText.setEditable(false);
            add(navnText);

            add(avdLabel);
            avdText.setText("" + r.getAvd());
            avdText.setEditable(false);
            add(avdText);

            add(fareLabel);
            String farlig = r.hasFarlig() == true ? "Ja" : "Nei";
            fareText.setText(farlig);
            fareText.setEditable(false);
            add(fareText);

            add(dyrLabel);
            String dyrene = "";
            ArrayList dyr = r.getDyr();
            for (Iterator idt = dyr.iterator(); idt.hasNext();) {
                Dyr next = (Dyr) idt.next();
                dyrene += next.getNavn() + " - " + next.getArt() + "\n";
            }
            dyrText.setText(dyrene);
            dyrText.setEditable(false);
            dyrText.setBackground(getBackground());
            dyrText.setBorder(fareText.getBorder());
            add(dyrText);
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel der man kan
     * endre et Rom objekt
     * @param Rom
     */
    class EndreRom extends JPanel {

        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel avdLabel = new JLabel("Avdeling:");
        private JComboBox avdCombo = new JComboBox();
        private JLabel fareLabel = new JLabel("Inneholder farlige dyr:");
        private JTextField fareText = new JTextField();
        private JLabel dyrLabel = new JLabel("Dyrene i dette rommet:");
        private JTextArea dyrText = new JTextArea();
        private JButton lagre = new JButton("Lagre endringer");
        private JButton slett = new JButton("Slett");
        private JPanel buttCont = new JPanel();
        private JTextArea errorField = new JTextArea("");
        private JCheckBox farlig = new JCheckBox();
        private JList dyrListe;
        private DefaultListModel lm;
        private Rom rom;

        public EndreRom(Rom r) {
            setLayout(new GridLayout(0, 2));
            rom = r;
            add(navnLabel);
            navnText.setText(r.getNavn());
            add(navnText);

            add(avdLabel);
            Avd current = r.getAvd();
            Avd north = Avd.valueOf("NORD");
            Avd south = Avd.valueOf("SØR");
            Avd east = Avd.valueOf("ØST");
            Avd west = Avd.valueOf("VEST");
            // legg nåværende avdeling øverst slik at den ikke endres ved et uhell
            avdCombo.addItem(current);
            // Bare legg til avdelinger dersom de ikke er den nåværende, da får vi ikke dobbelt opp.
            if (!current.equals(north)) {
                avdCombo.addItem(north);
            }
            if (!current.equals(south)) {
                avdCombo.addItem(south);
            }
            if (!current.equals(east)) {
                avdCombo.addItem(east);
            }
            if (!current.equals(west)) {
                avdCombo.addItem(west);
            }
            add(avdCombo);

            add(fareLabel);
            farlig.setSelected(r.hasFarlig());
            farlig.setText("Farlig");
            add(farlig);

            add(dyrLabel);
            String dyrene = "";
            ArrayList<Dyr> dyr = r.getDyr();
            lm = new DefaultListModel();
            for (Iterator it = dyr.iterator(); it.hasNext();) {
                Dyr next = (Dyr) it.next();

                lm.addElement(next);
                int indexOf = lm.indexOf(next);
            }
            dyrListe = new JList(lm);
            add(dyrListe);

            errorField.setLineWrap(true);
            errorField.setWrapStyleWord(true);
            add(errorField);
            lagre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    LagreRomActionPerformed(evt);
                }
            });
            slett.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    SlettRomActionPerformed(evt);
                }

                private void SlettRomActionPerformed(ActionEvent evt) {
                    String status = "";
                    if (rom.isEmpty()) {
                        romListe.remove(rom);
                        Dyrehage.lagreTilFil();
                        gui.treRebuilder();
                    } else {
                        status += "Kan ikke slette et rom med dyr i.";
                        errorField.setText(status);
                    }
                }
            });
            buttCont.add(lagre);
            buttCont.add(slett);
            add(buttCont);
        }

        /**
         * Metode for å lagre endringer gjort på rom.
         *
         * @param evt
         */
        private void LagreRomActionPerformed(ActionEvent evt) {
            String status = "";
            boolean save = true;

            String text = navnText.getText();
            if (!text.equals(rom.getNavn())) { // Hvis navnet er endret så lagrer vi endringen.
                try {
                    if (text.matches(_REGXSTRING)) {
                        status += "Navn satt til " + text + "\n";
                        rom.setNavn(text);
                    } else {
                        status += "Navnet inneholder ugyldige tegn.\n";
                        save = false;
                    }
                } catch (PatternSyntaxException ex) {
                    rom.setNavn("Midlertidig generisk romnavn");
                }
            }

            Avd avdeling = (Avd) avdCombo.getSelectedItem();
            if (!avdeling.equals(rom.getAvd())) { // Hvis avdeling er endret så lagerer vi endringen.
                if (avdeling instanceof Avd) {
                    status += "Avdeling satt til " + avdeling + "\n";
                    rom.setAvd(avdeling);
                } else {
                    status += "Avdeling er ikke gyldig, hvordan fikk du dette til?\n";
                    save = false;
                }
            }

            boolean fare = farlig.isSelected();
            if (fare != rom.hasFarlig()) { // hvis farlig er endret, så lagrer vi endringen.
                if (rom.getDyr().isEmpty()) {
                    String freText = fare ? "farlige" : "ikke farlige";
                    status += "Rommet er satt til å innholde dyr som er " + freText + "\n";
                    rom.setFarlig(fare);
                } else {
                    status += "Du kan ikke endre statusen på et rom inneholder dyr.\n";
                    save = false;
                }
            }

            if (save) {
                status += "Dyrehagen ble oppdatert.";
                errorField.setText(status);
                Dyrehage.lagreTilFil();

                gui.treRebuilder();

            } else {
                status += "Dyrehagen ble ikke oppdatert.";
                errorField.setText(status);
            }
        }
    }
}