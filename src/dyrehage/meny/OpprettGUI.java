/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage.meny;

import dyrehage.Dyrehage;
import dyrehage.Dyrehagen;
import dyrehage.dyr.Dyr;
import dyrehage.dyr.Fugl;
import dyrehage.dyr.KrypDyr;
import dyrehage.dyr.PatteDyr;
import dyrehage.rom.Rom;
import dyrehage.rom.RomInterface.Avd;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.PatternSyntaxException;
import javax.swing.*;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public class OpprettGUI extends JPanel {

    private JTabbedPane tabPanel = new JTabbedPane();
    private String oNavn = "";
    private ArrayList romListe;
    private ArrayList dyrListe;
    private JPanel oTab;
    private GUI gui;
    private final String _REGXSTRING = "(?i)[\\wæøå(),.\\-\\s]+";

    public OpprettGUI(Dyrehagen dh, Object obj, GUI gui) {


        // Opprett en liste over rom skal brukes i GUI
        romListe = dh.getRomListe();
        dyrListe = dh.getDyrListe();
        this.gui = gui;

        if (obj instanceof PatteDyr) {
            PatteDyr pd = (PatteDyr) obj;
            oTab = new OpprettDyr(pd);
            oNavn = "Opprett et nytt pattedyr";
        } else if (obj instanceof KrypDyr) {
            KrypDyr kd = (KrypDyr) obj;
            oTab = new OpprettDyr(kd);
            oNavn = "Opprett et nytt krypdyr";
        } else if (obj instanceof Fugl) {
            Fugl f = (Fugl) obj;
            oTab = new OpprettFugl(f);
            oNavn = "Opprett en ny fugl";
        } else if (obj instanceof Rom) {
            Rom r = (Rom) obj;
            oTab = new OpprettRom(r);
            oNavn = "Opprett et nytt rom";
        }

        oTab.setPreferredSize(new Dimension(578, 406));
        tabPanel.addTab(oNavn, oTab);
        tabPanel.setVisible(true);
        add(tabPanel);

    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om PatteDyr og KrypDyr objekter som kan redigeres og lagres til fil.
     * @param Dyr en subklasse av Dyr; PatteDyr eller KrypDyr
     */
    class OpprettDyr extends JPanel {

        private JLabel artLabel = new JLabel("Art:");
        private JTextField artText = new JTextField();
        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel farlLabel = new JLabel("Farlig:");
        private JLabel omrLabel = new JLabel("Leveområde:");
        private JTextField omrText = new JTextField();
        private JLabel prgLabel = new JLabel("Læreprogram:");
        private JTextField prgText = new JTextField();
        private JLabel plassLabel = new JLabel("Plassering:");
        private JComboBox plassBox = new JComboBox();
        private JButton lagre = new JButton("Lagre endringer");
        private JTextArea errorField = new JTextArea("");
        private JScrollPane scrlPane = new JScrollPane();
        private JCheckBox farligBox = new JCheckBox();
        private Dyr dyret;

        public OpprettDyr(Dyr dyrType) {

            setLayout(new GridLayout(0, 2));
            if (dyrType instanceof PatteDyr) {
                dyret = (PatteDyr) dyrType;
            } else if (dyrType instanceof KrypDyr) {
                dyret = (KrypDyr) dyrType;
            }

            add(artLabel);
            artText.setText("");
            add(artText);

            add(navnLabel);
            navnText.setText("");
            add(navnText);

            add(farlLabel);

            farligBox.setText("Farlig");
            add(farligBox);

            add(omrLabel);
            omrText.setText("");
            add(omrText);

            add(prgLabel);
            prgText.setText("");
            add(prgText);

            add(plassLabel);
            for (Iterator it = romListe.iterator(); it.hasNext();) {
                plassBox.addItem(it.next());
            }
            add(plassBox);

            scrlPane.setAutoscrolls(true);
            errorField.setEditable(false);
            scrlPane.setViewportView(errorField);
            add(scrlPane);

            lagre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    LagreDyrActionPerformed();
                }
            });
            add(lagre);
        }

        /**
         * Metode for å lagre endringer på dyr
         */
        private void LagreDyrActionPerformed() {
            String status = "";
            boolean save = true;

            String art = artText.getText(); // ---------- Art ----------//
            try {
                if (art.matches(_REGXSTRING)) {
                    status += "Art satt til " + art + ".\n";
                    dyret.setArt(art);
                } else {
                    status += "Art inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                dyret.setArt("Midlertidig generisk artsnavn");
            }

            String navn = navnText.getText(); // ---------- Navn ----------//
            try {
                if (navn.matches(_REGXSTRING)) {
                    status += "Navn satt til " + navn + ".\n";
                    dyret.setNavn(navn);
                } else {
                    status += "Navn inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                dyret.setNavn("Midlertidig generisk navn");
            }

            boolean erFarlig = farligBox.isSelected(); // ---------- Farlig? ----------//
            if (dyret instanceof PatteDyr) {
                PatteDyr pd = (PatteDyr) dyret;
                if (erFarlig == true || erFarlig == false) {
                    String fare = erFarlig ? "farlig" : "ikke farlig";
                    status += "Dyret er satt til et " + fare + " dyr.\n";
                    pd.setFarlig(erFarlig);
                } else {
                    save = false;
                    status += "Urm, hva??";
                }
            } else if (dyret instanceof KrypDyr) {
                KrypDyr kd = (KrypDyr) dyret;
                if (erFarlig == true || erFarlig == false) {
                    String fare = erFarlig ? "farlig" : "ikke farlig";
                    status += "Dyret er satt til et " + fare + " dyr.\n";
                    kd.setFarlig(erFarlig);
                } else {
                    save = false;
                    status += "Urm, hva??";
                }
            }

            String omrd = omrText.getText(); // ---------- Område ----------//
            try {
                if (omrd.matches(_REGXSTRING)) {
                    status += "Leveområde satt til " + omrd + ".\n";
                    dyret.setLeveomr(omrd);
                } else {
                    status += "Område inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                dyret.setNavn("Midlertidig generisk område");
            }

            String program = prgText.getText(); // ---------- Læreprogram ----------//
            try {
                if (program.matches(_REGXSTRING) || "".equals(program)) {
                    if ("".equals(program)) {
                        status += "Læreprogram satt til standardprogram.\n";
                    } else {
                        status += "Læreprogram satt til " + program + ".\n";
                        dyret.setLaereProg(program);
                    }
                } else {
                    status += "Læringsprogram inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                dyret.setNavn("Det er ikke opprettet noe læringsprogram for dette dyret enda.");
            }

            Rom rom = (Rom) plassBox.getSelectedItem(); // ---------- Plassering ----------//
            if (rom instanceof Rom) {
                if (rom.isEmpty() || erFarlig == rom.hasFarlig()) {
                    status += "Plassering satt til " + rom.getNavn() + ".\n";
                    dyret.setPlassering(rom);
                } else {
                    status += "Der er ikke lov å blande farlige dyr med dyr som ikke er farlige.\n";
                    save = false;
                }
            } else {
                status += "Ugyldig rom";
                save = false;
            }

            if (save) {  // Lagring av selve objektet skjer her -------------------------------------------------------- jeg er lett å finne.
                status += "Dyrehagen ble oppdatert.\n";
                errorField.setText(status);
                rom.addDyr(dyret);       // Legger dyret til rommet først når objektet lagres.
                rom.setFarlig(erFarlig);
                dyrListe.add(dyret);
                Dyrehage.lagreTilFil();

                gui.treRebuilder();

                if (dyret instanceof PatteDyr) {
                    dyret = new PatteDyr();
                } else if (dyret instanceof KrypDyr) {
                    dyret = new KrypDyr();
                }



            } else {
                status += "Dyrehagen ble ikke oppdatert.\n";
                errorField.setText(status);
            }
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel med informasjon
     * om et Fugl objekt som kan endres.
     * @param Fugl
     */
    class OpprettFugl extends JPanel {

        private JLabel artLabel = new JLabel("Art:");
        private JTextField artText = new JTextField();
        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel eggLabel = new JLabel("Egg:");
        private JLabel omrLabel = new JLabel("Leveområde:");
        private JTextField omrText = new JTextField();
        private JLabel prgLabel = new JLabel("Læreprogram:");
        private JTextField prgText = new JTextField();
        private JLabel plassLabel = new JLabel("Plassering:");
        private JComboBox plassBox = new JComboBox();
        private JButton lagre = new JButton("Lagre endringer");
        private JScrollPane scrlPane = new JScrollPane();
        private JTextArea errorField = new JTextArea("");
        private JPanel eggContainer = new JPanel();
        private JLabel minLabel = new JLabel("Minimum ant. egg");
        private JSpinner minSpinner = new JSpinner();
        private JLabel maxLabel = new JLabel("Maximum ant. egg");
        private JSpinner maxSpinner = new JSpinner();
        private Fugl fugl;

        public OpprettFugl(Fugl f) {

            setLayout(new GridLayout(0, 2));
            fugl = f;

            add(artLabel);
            artText.setText("");
            add(artText);

            add(navnLabel);
            navnText.setText("");
            add(navnText);

            add(eggLabel);
            minSpinner.setValue(0);
            maxSpinner.setValue(0);
            eggContainer.add(minLabel);
            eggContainer.add(minSpinner);
            eggContainer.add(maxLabel);
            eggContainer.add(maxSpinner);
            add(eggContainer);

            add(omrLabel);
            omrText.setText("");
            add(omrText);

            add(prgLabel);
            prgText.setText("");
            add(prgText);

            add(plassLabel);
            for (Iterator it = romListe.iterator(); it.hasNext();) {
                plassBox.addItem(it.next());
            }
            add(plassBox);

            scrlPane.setAutoscrolls(true);
            errorField.setEditable(false);
            scrlPane.setViewportView(errorField);
            add(scrlPane);

            lagre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    LagreFuglActionPerformed();
                }
            });
            add(lagre);
        }

        /**
         * Metode for å endre Fugl objektet
         *
         * @param evt
         */
        private void LagreFuglActionPerformed() {
            String status = "";
            boolean save = true;

            String art = artText.getText(); // ---------- Art ----------//
            try {
                if (art.matches(_REGXSTRING)) {
                    status += "Art satt til " + art + ".\n";
                    fugl.setArt(art);
                } else {
                    status += "Art inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                fugl.setNavn("Midlertidig generisk artsnavn");
            }

            String navn = navnText.getText(); // ---------- Navn ----------//
            try {
                if (navn.matches(_REGXSTRING)) {
                    status += "Navn satt til " + navn + ".\n";
                    fugl.setNavn(navn);
                } else {
                    status += "Navn inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                fugl.setNavn("Midlertidig generisk navn");
            }

            int minEgg = (int) minSpinner.getValue(); // ---------- Egg ----------//
            int maxEgg = (int) maxSpinner.getValue();

            if (minEgg < maxEgg) {
                status += "Minimum egg er satt til " + minEgg + ".\n";
                fugl.setMinEgg(minEgg);
            } else if (minEgg > maxEgg) {
                status += "Kan ikke sette minimum egg høyere enn maksimum egg.\n";
                save = false;
            }
            if (maxEgg != fugl.getMaxEgg() && maxEgg > minEgg) {
                status += "Maksimum egg satt til " + maxEgg + ".\n";
                fugl.setMaxEgg(maxEgg);
            } else if (maxEgg < minEgg) {
                status += "Kan ikke sette maksimum egg lavere en minimum egg.\n";
                save = false;
            }

            String omrd = omrText.getText(); // ---------- Leveområde ----------//
            try {
                if (omrd.matches(_REGXSTRING)) {
                    status += "Leveområde satt til " + omrd + ".\n";
                    fugl.setLeveomr(omrd);
                } else {
                    status += "Område inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                fugl.setNavn("Midlertidig generisk område");
            }

            String program = prgText.getText(); // ---------- læreprogram ----------//
            try {
                if (program.matches(_REGXSTRING) || "".equals(program)) {
                    if("".equals(program)){
                        status += "Læreprogram satt til standardprogram.\n";
                    } else {
                        status += "Læreprogram satt til " + program + ".\n";
                        fugl.setLaereProg(program);
                    }
                } else {
                    status += "Læringsprogram inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                fugl.setNavn("Det er ikke opprettet noe læringsprogram for dette dyret enda.");
            }

            Rom rom = (Rom) plassBox.getSelectedItem(); // ---------- Plassering ----------//
            if (rom instanceof Rom) {
                status += "Plassering satt til " + rom.getNavn() + ".\n";
                fugl.setPlassering(rom);
            } else {
                status += "Ugyldig rom";
                save = false;
            }

            if (save) {// Lagring av selve objektet skjer her ------------------------------------------------------jeg er lett å finne. ---//
                status += "Dyrehagen ble oppdatert.\n";
                errorField.setText(status);
                rom.addDyr(fugl); //legger dyret til rommet først når objektet lagres
                dyrListe.add(fugl);
                Dyrehage.lagreTilFil();
                fugl = new Fugl();

                gui.treRebuilder();

            } else {
                status += "Dyrehagen ble ikke oppdatert.\n";
                errorField.setText(status);
            }
        }
    }

    /**
     *
     * @author Mikael Gyth - gythmikael@gmail.com Viser et panel der man kan
     * endre et Rom objekt
     * @param Rom
     */
    class OpprettRom extends JPanel {

        private JLabel navnLabel = new JLabel("Navn:");
        private JTextField navnText = new JTextField();
        private JLabel avdLabel = new JLabel("Avdeling:");
        private JComboBox avdCombo = new JComboBox();
        private JLabel fareLabel = new JLabel("Inneholder farlige dyr:");
        private JButton lagre = new JButton("Lagre endringer");
        private JTextArea errorField = new JTextArea("");
        private JCheckBox farlig = new JCheckBox();
        private Rom rom;

        public OpprettRom(Rom r) {

            setLayout(new GridLayout(0, 2));
            rom = r;

            add(navnLabel);
            navnText.setText("");
            add(navnText);

            add(avdLabel);
            Avd north = Avd.valueOf("NORD");
            Avd south = Avd.valueOf("SØR");
            Avd east = Avd.valueOf("ØST");
            Avd west = Avd.valueOf("VEST");


            avdCombo.addItem(north);
            avdCombo.addItem(south);
            avdCombo.addItem(east);
            avdCombo.addItem(west);

            add(avdCombo);

            add(fareLabel);
            farlig.setSelected(false);
            farlig.setText("Farlig");
            add(farlig);

            add(errorField);
            lagre.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                    LagreRomActionPerformed();
                }
            });
            add(lagre);
        }

        /**
         * Metode for å lagre rom.
         *
         * @param evt
         */
        private void LagreRomActionPerformed() {
            String status = "";
            boolean save = true;

            String text = navnText.getText(); // ---------- Navn ----------//
            try {
                if (text.matches(_REGXSTRING)) {
                    status += "Navn satt til " + text + "\n";
                    rom.setNavn(text);
                } else {
                    status += "Navnet inneholder ugyldige tegn.\n";
                    save = false;
                }
            } catch (PatternSyntaxException ex) {
                rom.setNavn("Midlertidig generisk romnavn");
            }

            Avd avdeling = (Avd) avdCombo.getSelectedItem(); // ---------- Avdeling ----------//
            if (avdeling instanceof Avd) {
                status += "Avdeling satt til " + avdeling + "\n";
                rom.setAvd(avdeling);
            } else {
                status += "Avdeling er ikke gyldig, hvordan fikk du dette til?\n";
                save = false;
            }

            boolean fare = farlig.isSelected(); // ---------- Inneholder farlige dyr ----------//
            if (fare == true || fare == false) {
                String freText = fare ? "farlige" : "ikke farlige";
                status += "Rommet er satt til å innholde dyr som er " + freText + "\n";
                rom.setFarlig(fare);
            } else {
                status += "Seriøst, hva er det du holder på med?\n";
                save = false;
            }

            if (save) {  // Lagring av selve objektet skjer her ---------------------------------------------------------------- jeg er lett å finne.
                status += "Dyrehagen ble oppdatert.";
                errorField.setText(status);
                romListe.add(rom);
                Dyrehage.lagreTilFil();
                rom = new Rom();

                gui.treRebuilder();
            } else {
                status += "Dyrehagen ble ikke oppdatert.\n";
                errorField.setText(status);
            }
        }
    }
}