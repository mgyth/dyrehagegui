/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dyrehage.meny;

import dyrehage.Dyrehage;
import dyrehage.Dyrehagen;
import dyrehage.dyr.Dyr;
import dyrehage.dyr.Fugl;
import dyrehage.dyr.KrypDyr;
import dyrehage.dyr.PatteDyr;
import dyrehage.rom.Rom;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author Mikael Gyth - gythmikael@gmail.com
 */
public final class GUI extends JFrame {

    //Dyrehageobjektet
    private Dyrehagen dh;
    private JMenu dyr = new JMenu();
    private JMenu fugl = new JMenu();
    private JMenu krypdyr = new JMenu();
    private JMenu pattedyr = new JMenu();
    private JMenu rom = new JMenu();
    private JMenu fil = new JMenu();
    private JMenuItem avslutt = new JMenuItem();
    private JMenuItem lagre = new JMenuItem();
    private JMenuItem opprettRom = new JMenuItem();
    private ArrayList romListe = new ArrayList();
    private ArrayList dyrListe = new ArrayList();
    private JPanel mainPanel = new JPanel(new BorderLayout());
    private JMenuItem opprettKrypDyr = new JMenuItem();
    private JMenuItem opprettPatteDyr = new JMenuItem();
    private JMenuItem opprettFugl = new JMenuItem();
    final JFrame frame = this;
    public static JPanel leftPanel = new JPanel(new BorderLayout());
    private JTree treet;

    public JMenuBar byggMeny() {
        final JMenuBar menyBar = new JMenuBar();

        romListe = dh.getRomListe();
        dyrListe = dh.getDyrListe();

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        fil.setText("Fil");

        lagre.setText("Lagre");
        lagre.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                lagreActionPerformed(evt);
            }
        });
        fil.add(lagre);

        avslutt.setText("Avslutt");
        avslutt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                avsluttActionPerformed(evt);
            }
        });
        fil.add(avslutt);

        menyBar.add(fil);

        rom.setText("Rom");



        opprettRom.setText("Opprett");
        opprettRom.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                opprettActionPerformed(new Rom());
            }
        });
        rom.add(opprettRom);

        menyBar.add(rom);

        dyr.setText("Dyr");

        pattedyr.setText("Pattedyr");
        opprettPatteDyr.setText("Opprett");
        opprettPatteDyr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                opprettActionPerformed(new PatteDyr());
            }
        });
        pattedyr.add(opprettPatteDyr);

        dyr.add(pattedyr);

        krypdyr.setText("Krypdyr");
        opprettKrypDyr.setText("Opprett");
        opprettKrypDyr.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                opprettActionPerformed(new KrypDyr());
            }
        });
        krypdyr.add(opprettKrypDyr);

        dyr.add(krypdyr);

        fugl.setText("Fugl");
        opprettFugl.setText("Opprett");
        opprettFugl.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                opprettActionPerformed(new Fugl());
            }
        });
        fugl.add(opprettFugl);

        dyr.add(fugl);


        menyBar.add(dyr);

        return menyBar;
    }

    public GUI() {
    }

    public GUI(Dyrehagen dyrehageObjektet) {
        dh = dyrehageObjektet;

        JMenuBar mBar = byggMeny();
        mBar.revalidate();
        frame.setJMenuBar(mBar);

        setPreferredSize(new Dimension(850, 500));
        setLayout(new BorderLayout());

        leftPanel.setPreferredSize(new Dimension(250, 480));
        leftPanel.setVisible(true);
        leftPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        treet = buildTree();
        leftPanel.add(treet);
        add(leftPanel, BorderLayout.WEST);
        
        MainP mp = new MainP(dh);
        mainPanel.add(mp);
        
        mainPanel.setPreferredSize(new Dimension(600, 480));
        add(mainPanel, BorderLayout.CENTER);

        pack();
    }

    /**
     * Actionlistener for oppretting, har bare en felles og så velges typen ut
     * ifra objektet som sendes inn.
     *
     * @param obj av typen PatteDyr, KrypDyr, Fugl eller Rom.
     */
    private void opprettActionPerformed(Object obj) {
        OpprettGUI oGUI = new OpprettGUI(dh, obj, this);
        oGUI.setPreferredSize(new Dimension(250, 210)); // sett størrelsen på vg panelet
        mainPanel.removeAll();                          // tøm mainPanel for innhold
        mainPanel.add(oGUI, BorderLayout.CENTER);       //legg til det nye panelet
        mainPanel.revalidate();                         // beregn layout på nytt
        mainPanel.repaint();                            // tegn layout på nytt
    }

    // ActionListener for å se på dyr eller rom
    private void visActionPerformed(Object objektet) {

        VisGUI vg = new VisGUI(objektet, romListe, dyrListe, this); //opprett et nytt vg objekt med data fra objektet

        vg.setPreferredSize(new Dimension(250, 210)); // sett størrelsen på vg panelet

        mainPanel.removeAll();                   // tøm mainPanel for innhold
        mainPanel.add(vg, BorderLayout.CENTER);  //legg til det nye panelet
        mainPanel.revalidate();                  // beregn layout på nytt
        mainPanel.repaint();                     // tegn layout på nytt
    }
    
    // ActionListener for å se på dyr eller rom
    private void visDyrehagenPerformed() {

        MainP mp = new MainP(dh);

        mainPanel.removeAll();                   // tøm mainPanel for innhold
        mainPanel.add(mp, BorderLayout.CENTER);  //legg til det nye panelet
        mainPanel.revalidate();                  // beregn layout på nytt
        mainPanel.repaint();                     // tegn layout på nytt
    }

    // ActionListener for å avslutte kjøringen av programmet
    private void avsluttActionPerformed(ActionEvent evt) {
        //Dyrehage.lagreTilFil();
        System.exit(0);
    }

    // ActionListener for å lagre objektet til fil.
    private void lagreActionPerformed(ActionEvent evt) {
        Dyrehage.lagreTilFil();
    }

    public JTree buildTree() {
        final JTree tre;

        DefaultMutableTreeNode top = new DefaultMutableTreeNode("Dyrehagen");
        createNodes(top);
        tre = new JTree(top);
        tre.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tre.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent evt) {
                klickEvent(evt);
            }

            private void klickEvent(MouseEvent evt) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) tre.getLastSelectedPathComponent();

                if (node == null) //Nothing is selected.     
                {
                    return;
                }

                Object nodeInfo = node.getUserObject();
                if (node.isLeaf()) {
                    if (nodeInfo instanceof Dyr) {
                        Dyr dyr = (Dyr) nodeInfo;
                        System.out.println("dyr = " + dyr);
                        visActionPerformed(dyr);
                    } else if (nodeInfo instanceof Rom) {
                        Rom rom = (Rom) nodeInfo;
                        System.out.println("rom = " + rom);
                        visActionPerformed(rom);
                    }
                } else if(node.isRoot()){
                    visDyrehagenPerformed();
                } else {
                    if (nodeInfo instanceof Dyr) {
                        Dyr dyr = (Dyr) nodeInfo;
                        System.out.println("dyr = " + dyr);
                        visActionPerformed(dyr);
                    } else if (nodeInfo instanceof Rom) {
                        Rom rom = (Rom) nodeInfo;
                        System.out.println("rom = " + rom);
                        visActionPerformed(rom);
                    }
                }
            }
        });
        return tre;
    }

    private void createNodes(DefaultMutableTreeNode top) {
        DefaultMutableTreeNode rom = null;
        DefaultMutableTreeNode dyr = null;

        for (Iterator romIt = romListe.iterator(); romIt.hasNext();) {
            Rom nextRom = (Rom) romIt.next();
            rom = new DefaultMutableTreeNode(nextRom);

            top.add(rom);
            ArrayList dyrene = nextRom.getDyr();
            for (Iterator dyrIt = dyrene.iterator(); dyrIt.hasNext();) {
                Dyr nextDyr = (Dyr) dyrIt.next();
                dyr = new DefaultMutableTreeNode(nextDyr);
                rom.add(dyr);
            }
        }




    }

    public void treRebuilder() {
        System.out.println("treRebuildFire\n");
        GUI.leftPanel.removeAll();
        JTree reTreet = buildTree();
        GUI.leftPanel.add(reTreet);
        GUI.leftPanel.revalidate();
        GUI.leftPanel.repaint();
    }
}
